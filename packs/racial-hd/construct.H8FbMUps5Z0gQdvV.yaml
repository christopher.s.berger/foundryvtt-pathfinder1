_id: H8FbMUps5Z0gQdvV
_key: '!items!H8FbMUps5Z0gQdvV'
img: systems/pf1/icons/skills/mech_7.jpg
name: Construct
system:
  bab: high
  changes:
    - _id: bo1uudua
      formula: (max(0, @size - 3) * 10) + (max(0, @size - 6) * 10)
      target: mhp
      type: untypedPerm
  description:
    value: >-
      <p>A construct is an animated object or artificially created creature.</p>

      <h2>Features</h2>

      <p>A construct has the following features.</p>

      <ul>

      <li>d10 Hit Die.</li>

      <li>Base attack bonus equal to total Hit Dice (fast progression).</li>

      <li>No good saving throws.</li>

      <li>Skill points equal to 2 + Int modifier (minimum 1) per Hit Die.
      However, most constructs are mindless and gain no skill points or feats.
      Constructs do not have any class skills, regardless of their
      <em>Intelligence</em> scores.</li>

      <li>Construct Size Bonus Hit Points Fine — Diminutive — Tiny — Small 10
      Medium 20 Large 30 Huge 40 Gargantuan 60 Colossal 80</li>

      </ul>

      <h2>Traits</h2>

      <p>A construct possesses the following traits (unless otherwise noted in a
      creature’s entry).</p>

      <ul>

      <li>No Constitution score. Any DCs or other statistics that rely on a
      Constitution score treat a construct as having a score of 10 (no bonus or
      penalty).</li>

      <li>Low-light vision.</li>

      <li>Darkvision 60 feet.</li>

      <li>Immunity to all mind-affecting effects (charms, compulsions, morale
      effects, patterns, and phantasms).</li>

      <li>Immunity to disease, death effects, necromancy effects, paralysis,
      poison, sleep effects, and stunning.</li>

      <li>Cannot heal damage on its own, but often can be repaired via exposure
      to a certain kind of effect (see the creature’s description for details)
      or through the use of the <em>Craft Construct</em> feat. Constructs can
      also be healed through spells such as <em>make whole</em>. A construct
      with the fast healing special quality still benefits from that
      quality.</li>

      <li>Not subject to <em>ability damage</em>, <em>ability drain</em>,
      fatigue, exhaustion, energy drain, or nonlethal damage.</li>

      <li>Immunity to any effect that requires a <em>Fortitude</em> save (unless
      the effect also works on objects, or is harmless).</li>

      <li>Not at risk of death from massive damage. Immediately destroyed when
      reduced to 0 hit points or less.</li>

      <li>A construct cannot be raised or resurrected.</li>

      <li>A construct is hard to destroy, and gains bonus hit points based on
      size, as shown on the following table.</li>

      </ul>

      <ul>

      <li>Proficient with its natural weapons only, unless generally humanoid in
      form, in which case proficient with any weapon mentioned in its
      entry.</li>

      <li>Proficient with no armor.</li>

      <li>Constructs do not breathe, eat, or sleep.</li>

      </ul>

      <h2>Construct Size Bonus Hit Points</h2>

      <table border="1">

      <tbody>

      <tr>

      <td>Fine</td>

      <td>10</td>

      </tr>

      <tr>

      <td>Diminutive</td>

      <td>10</td>

      </tr>

      <tr>

      <td>Tiny</td>

      <td>10</td>

      </tr>

      <tr>

      <td>Small</td>

      <td>10</td>

      </tr>

      <tr>

      <td>Medium</td>

      <td>20</td>

      </tr>

      <tr>

      <td>Large</td>

      <td>30</td>

      </tr>

      <tr>

      <td>Huge</td>

      <td>40</td>

      </tr>

      <tr>

      <td>Gargantuan</td>

      <td>60</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td>80</td>

      </tr>

      </tbody>

      </table>
  hd: 10
  hp: 10
  skillsPerLevel: 2
  subType: racial
  tag: construct
type: class

