_id: HrbdB5h8EiWyMHo5
_key: '!items!HrbdB5h8EiWyMHo5'
img: systems/pf1/icons/skills/violet_07.jpg
name: Tsukumogami
system:
  changes:
    - _id: 9jbfurrt
      formula: '2'
      target: nac
      type: untyped
  crOffset: '2'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>When an object reaches the 100-year
      anniversary of its crafting, sometimes it forms an amalgam with a <a
      href="https://aonprd.com/MonsterFamilies.aspx?ItemName=Kami">kami</a>,
      creating a creature known as a tsukumogami. Tsukumogami run the gamut in
      personality, outlook, and function. Objects that are well kept and cared
      for often form curious and helpful tsukumogami. Most commonly, tsukumogami
      are mischievous and frightening but not actually malign. Tsukumogami
      formed from objects that have been abandoned, neglected, or misused are
      dangerous both to the humans around and to themselves, as their
      uncontrolled rage might eventually transform them into <a
      href="https://aonprd.com/MonsterFamilies.aspx?ItemName=Oni">oni</a>.<p>Tsukumogami
      is an acquired template that can be added to any animated object (referred
      to hereafter as the base creature). A tsukumogami retains all the base
      creature’s statistics and special abilities except as noted
      here.<p><b>Challenge Rating:</b> Base creature’s CR +
      2.<p><b>Alignment:</b> Any.<p><b>Type:</b> The creature’s type changes to
      outsider (kami, native). Tsukumogami have good Reflex and Will saves, so
      increase the base creature’s Reflex and Will saves to 2 + 1/2 its Hit Dice
      + the relevant ability modifier.<p><b>Armor Class:</b> A tsukumogami’s
      natural armor bonus increases by 2.<p><b>Hit Dice:</b> Retain the base
      creature’s construct bonus hit points from size (if any). As outsiders,
      tsukumogami gain bonus hit points from high Constitution
      scores.<p><b>Spell-Like Abilities:</b> Tsukumogami gain spell-like
      abilities based on their size, usable at will. The caster level is equal
      to the tsukumogami’s Hit Dice.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>Size</b></td>

      <td><b>Abilities</b></td>

      </tr>

      <tr>

      <td>Tiny+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Decrepit%20disguise"><em>Decrepit
      disguise</em></a> (self only), <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=ghost%20sound"><em>ghost
      sound</em></a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=mending"><em>mending</em></a>,
      <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=quintessence"><em>quintessence</em></a>
      (self only)</td>

      </tr>

      <tr>

      <td>Small+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Invisibility"><em>Invisibility</em></a>
      (self only), <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=ventriloquism"><em>ventriloquism</em></a></td>

      </tr>

      <tr>

      <td>Medium+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Levitate"><em>Levitate</em></a>,
      <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=silent%20image"><em>silent
      image</em></a></td>

      </tr>

      <tr>

      <td>Large+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Fly"><em>Fly</em></a>,
      <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=make%20whole"><em>make
      whole</em></a></td>

      </tr>

      <tr>

      <td>Huge+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Obscure%20object"><em>Obscure
      object</em></a> (self only), <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=shrink%20item"><em>shrink
      item</em></a> (self only, no volume limit)</td>

      </tr>

      <tr>

      <td>Gargantuan+</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Animate%20objects"><em>Animate
      objects</em></a> (each casting ends any previous castings)</td>

      </tr>

      <tr>

      <td>Colossal</td>

      <td><a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=Sympathy"><em>Sympathy</em></a>
      (self only)</td>

      </tr>

      </tbody>

      </table>

      <p><br><b>Special Qualities and Defensive Abilities:</b> Because it grows
      additional features such as a tongue, arms, or legs, a tsukumogami gains
      the additional attack animated object quality without spending
      Construction Points, and all its attacks increase their damage dice by one
      step. A tsukumogami can gain 10 bonus hit points as an additional option
      costing 1 CP. It gains the freeze special quality. As kami, tsukumogami
      gain immunity to petrification and polymorph effects; resist acid 10,
      electricity 10, and fire 10; telepathy 100 feet; fast healing 5; merge
      with ward; and ward. Though a tsukumogami loses its construct type, it
      keeps its hardness, low-light vision, and all its construct immunities. It
      can still be affected by spells that affect objects or constructs. A
      tsukumogami is always merged with its ward, and unlike most kami, it forms
      an amalgam with its ward, so it can move and communicate while
      merged.<p><b>Ability Scores:</b> A tsukumogami has a 15 Intelligence, 17
      Wisdom, and 14 Charisma. A Medium tsukumogami receives a +4 bonus to
      Strength, a +4 bonus to Dexterity, and a Constitution score of 19. These
      ability scores are adjusted for size.<p><b>Skills:</b> A tsukumogami has a
      number of skill points per racial Hit Die equal to 6 + its Intelligence
      modifier. Its racial class skills are the base outsider class skills plus
      Disguise, Knowledge (history), Perform (any one), and Sleight of Hand.</p>
  subType: template
type: feat

