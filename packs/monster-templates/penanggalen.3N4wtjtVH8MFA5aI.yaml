_id: 3N4wtjtVH8MFA5aI
_key: '!items!3N4wtjtVH8MFA5aI'
img: systems/pf1/icons/skills/violet_07.jpg
name: Penanggalen
system:
  changes:
    - _id: 98nskk8w
      formula: '6'
      target: nac
      type: untyped
    - _id: sxl5nyik
      formula: '6'
      target: str
      type: untyped
    - _id: p59632xs
      formula: '4'
      target: dex
      type: untyped
    - _id: 6r836utv
      formula: '2'
      target: int
      type: untyped
    - _id: t2haj2jw
      formula: '2'
      target: wis
      type: untyped
    - _id: omry38fu
      formula: '4'
      target: cha
      type: untyped
    - _id: 1iwrjpl5
      formula: '8'
      target: skill.blf
      type: racial
    - _id: rr2p1uxw
      formula: '8'
      target: skill.fly
      type: racial
    - _id: 8ughhjxo
      formula: '8'
      target: skill.kar
      type: racial
    - _id: 6t7plg4u
      formula: '8'
      target: skill.per
      type: racial
    - _id: y6ym4nyj
      formula: '8'
      target: skill.sen
      type: racial
    - _id: cp4q4d79
      formula: '8'
      target: skill.ste
      type: racial
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>The hideous penanggalen is one of
      the most horrific vampiric monstrosities. By day, a penanggalen appears to
      be a normal humanoid, but at night or when provoked, the creature’s head
      rips free from the rest of her body, coils of viscera and entrails
      dangling from her throat as she launches into the air, seeking blood to
      sate her unholy thirst.<p>Unlike most undead, the penanggalen is more akin
      to the lich in that she willfully abandons both her mortality and morality
      to become a hideous undead monster. While penanggalens are traditionally
      female spellcasters, any creature capable of performing the vile ritual of
      transformation can become one.<p>Similar to a lich, a creature works
      toward becoming a penanggalen. More than one such transformation ritual
      exists, but all require heinous acts that symbolize the casting aside of
      kindness, benevolence, and any semblance of feelings other than cruelty.
      Many of these rituals call for the repeated consumption of blood, bile,
      tears, and other fluids drawn from captured and tortured innocents.<p>A
      penanggalen keeps a vat of vinegar in her lair. When returning from a
      night of feeding, a penanggalen’s organs are swollen with blood. In order
      to fit back into her body, the penanggalen must soak for 1 hour in this
      vat of vinegar. Once reduced, a penanggalen slides back into her body. If
      a penanggalen is slain away from her body, the body rapidly deteriorates
      into foul-smelling grit.<p>The penanggalen presented above was a witch in
      life. The witch class is presented in full in the Advanced Player’s
      Guide.<p>“Penanggalen” is an acquired template that can be added to any
      living creature with 5 or more Hit Dice (referred to hereafter as the base
      creature). Most penanggalens were once humanoids or monstrous humanoids
      and nearly every penanggalen is female. A penanggalen uses the base
      creature’s stats and abilities except as noted here.<p><b>CR:</b> Same as
      base creature +1.</p>

      <p><b>Alignment:</b> Any evil.</p>

      <p><b>Type:</b> The creature’s type changes to undead (augmented). Do not
      recalculate class Hit Dice, BAB, or saves.</p>

      <p><b>Senses:</b> A penanggalen gains darkvision 60 ft.</p>

      <p><b>Armor Class:</b> Natural armor improves by +6.</p>

      <p><b>Hit Dice:</b> Change the base creature’s racial HD to d8s. All HD
      derived from class levels remain unchanged. As undead, a penanggalen uses
      her Charisma modifier to determine bonus hit points (instead of
      Constitution).</p>

      <p><b>Defensive Abilities:</b> A penanggalen gains channel resistance +4,
      DR 5/silver and slashing, resistance to cold 10 and fire 10, and all of
      the defensive abilities granted by the undead type. A penanggalen also
      gains fast healing 5.</p>

      <p><b>Weaknesses:</b> A penanggalen gains light sensitivity. In addition,
      a penanggalen is staggered while outside of her human body and exposed to
      direct sunlight.</p>

      <p><b>Speed:</b> When a penanggalen is attached to her body, she retains
      the same base speed as the base creature. When a penanggalen is separated
      from her body, she has only a fly speed of 60 feet with good
      maneuverability.</p>

      <p><b>Melee:</b> A penanggalen gains a bite attack and a slam attack when
      she is detached from her body. Damage is standard for attacks of these
      types for the penanggalen’s size. Both natural attacks are treated as
      magic for the purpose of overcoming damage reduction.</p>

      <p><b>Special Attacks:</b> A penanggalen retains all of the base
      creature’s special attacks. She also gains the following additional
      special attacks. Save DCs are equal to 10 + 1/2 the penanggalen’s HD + the
      penanggalen’s Charisma modifier unless otherwise noted.<p><em>Blood Drain
      (Su): </em>A penanggalen’s blood drain special attack causes 1d4
      Constitution damage.</p>

      <p><em>Create Spawn (Su): </em>When a penanggalen slays a female humanoid
      via blood drain, and if that slain humanoid had at least 10 Hit Dice in
      life, that slain humanoid rises as a manananggal at the next sunset. This
      manananggal is under the command of the penanggalen who created it, and
      remains enslaved until that penanggalen’s destruction. A penanggalen may
      have enslaved spawn totalling no more than twice its own Hit Dice; any
      spawn it creates that would exceed this limit become free-willed
      undead.</p>

      <p><em>Disease (Su): </em>Any creature a penanggalen bites is exposed to
      filth fever—the save DC against this disease is set by the penanggalen,
      not the disease itself.</p>

      <p><em>Wither (Su): </em>A penanggalen’s entrails drip with a foul bile
      that blisters and weakens living flesh. Any creature that is damaged by a
      penanggalen’s slam attack must succeed at a Fortitude save or take 1d4
      Dexterity and 1d4 Charisma damage.<p><b>Ability Scores:</b> Str +6, Dex
      +4, Int +2, Wis +2, Cha +4. As an undead creature, a penanggalen has no
      Constitution score.</p>

      <p><b>Skills:</b> A penanggalen gains a +8 racial bonus on Bluff, Fly,
      Knowledge (arcana), Perception, Sense Motive, and Stealth checks.</p>

      <p><b>Special Qualities:</b> A penanggalen gains the following special
      quality.<p><em>Separate (Su): </em>During the day, a penanggalen has the
      same appearance as she did in life, and does not detect as undead (though
      she is still an undead creature). At night, she can detach her head and
      entrails as a full-round action. Her physical but now hollow body exists
      as dead flesh, but can be destroyed if it suffers damage equal to the
      penanggalen’s normal hit point total. Before a penanggalen can return to
      her body, she must soak her entrails in vinegar for 1 hour—she can then
      reattach to her body, at which point any damage done to the body
      immediately heals (although damage the penanggalen herself suffered
      remains). A penanggalen whose body is destroyed can never again walk the
      day in living form, but is otherwise unharmed (save for no longer having a
      safe way to travel in direct sunlight). When a penanggalen wears her body,
      she cannot use her natural attacks, her fly speed, or any of her special
      penanggalen attacks.</p>
  subType: template
type: feat

