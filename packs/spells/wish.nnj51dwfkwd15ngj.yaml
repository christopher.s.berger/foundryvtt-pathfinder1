_id: nnj51dwfkwd15ngj
_key: '!items!nnj51dwfkwd15ngj'
img: icons/magic/symbols/runes-star-pentagon-blue.webp
name: Wish
system:
  actions:
    - _id: degg0fykrqd5mb69
      actionType: spellsave
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      area: see text
      duration:
        units: seeText
      name: Use
      range:
        units: seeText
      save:
        description: none; see text
      spellEffect: see text
      target:
        value: see text
  components:
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p><em>Wish is</em> the mightiest spell a wizard or sorcerer can cast. By
      simply speaking aloud, you can alter reality to better suit you. Even
      <em>wish,</em> however, has its limits. A <em>wish</em> can produce any
      one of the following effects.</p><ul><li><p>Duplicate any sorcerer/wizard
      spell of 8th level or lower, provided the spell does not belong to one of
      your opposition schools.</p></li><li><p>Duplicate any non-sorcerer/wizard
      spell of 7th level or lower, provided the spell does not belong to one of
      your opposition schools.</p></li><li><p>Duplicate any sorcerer/wizard
      spell of 7th level or lower, even if it belongs to one of your opposition
      schools.</p></li><li><p>Duplicate any non-sorcerer/wizard spell of 6th
      level or lower, even if it belongs to one of your opposition
      schools.</p></li><li><p>Undo the harmful effects of many other spells,
      such as <em>geas/ quest</em> or <em>insanity.</em></p></li><li><p>Grant a
      creature a +1 inherent bonus to an ability score. Two to five
      <em>wish</em> spells cast in immediate succession can grant a creature a
      +2 to +5 inherent bonus to an ability score (two <em>wish</em>es for a +2
      inherent bonus, three <em>wish</em>es for a +3 inherent bonus, and so
      on).</p><p>Inherent bonuses are instantaneous, so they cannot be
      dispelled.</p><p><em>Note:</em> An inherent bonus may not exceed +5 for a
      single ability score, and inherent bonuses to a particular ability score
      do not stack, so only the best one applies.</p></li><li><p>Remove injuries
      and afflictions. A single <em>wish</em> can aid one creature per caster
      level, and all subjects are cured of the same kind of affliction. For
      example, you could heal all the damage you and your companions have taken,
      or remove all poison effects from everyone in the party, but not do both
      with the same <em>wish</em>.</p></li><li><p>Revive the dead. A
      <em>wish</em> can bring a dead creature back to life by duplicating a
      <em>resurrection</em> spell. A <em>wish</em> can revive a dead creature
      whose body has been destroyed, but the task takes two <em>wish</em>es: one
      to recreate the body and another to infuse the body with life again. A
      <em>wish</em> cannot prevent a character who was brought back to life from
      gaining a permanent negative level.</p></li><li><p>Transport travelers. A
      <em>wish</em> can lift one creature per caster level from anywhere on any
      plane and place those creatures anywhere else on any plane regardless of
      local conditions. An unwilling target gets a Will save to negate the
      effect, and spell resistance (if any) applies.</p></li><li><p>Undo
      misfortune. A <em>wish</em> can undo a single recent event.</p><p>The
      <em>wish</em> forces a reroll of any roll made within the last round
      (including your last turn). Reality reshapes itself to accommodate the new
      result. For example, a <em>wish</em> could undo an opponent's successful
      save, a foe's successful critical hit (either the attack roll or the
      critical roll), a friend's failed save, and so on. The reroll, however,
      may be as bad as or worse than the original roll. An unwilling target gets
      a Will save to negate the effect, and spell resistance (if any)
      applies.</p></li></ul><p>You may try to use a <em>wish</em> to produce
      greater effects than these, but doing so is dangerous. (The <em>wish</em>
      may pervert your intent into a literal but undesirable fulfilment or only
      a partial fulfilment, at the GM's discretion.) Duplicated spells allow
      saves and spell resistance as normal (but save DCs are for 9th-level
      spells).</p><p>When a <em>wish</em> duplicates a spell with a material
      component that costs more than 10,000 gp, you must provide that component
      (in addition to the 25,000 gp diamond component for this spell).</p>
  learnedAt:
    bloodline:
      Arcane: 9
      Div: 9
      Djinni: 9
      Draconic: 9
      Efreeti: 9
      Impossible: 9
      Marid: 9
      Shaitan: 9
    class:
      arcanist: 9
      psychic: 9
      sorcerer: 9
      wizard: 9
  level: 9
  materials:
    gpValue: 25000
    value: diamond worth 25,000 gp
  school: uni
  sources:
    - id: PZO1110
      pages: '370'
type: spell

