_id: 9ri40ee00k8brat1
_key: '!items!9ri40ee00k8brat1'
img: systems/pf1/icons/misc/magic-swirl.png
name: Sacrifice
system:
  actions:
    - _id: rnfe0gyvf0boptwg
      activation:
        type: minute
        unchained:
          type: minute
      duration:
        units: spec
        value: instantaneous, 1 hour, or 1 day (see text)
      name: Use
      range:
        units: close
      target:
        value: one summoned elemental or outsider (see text)
  components:
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>You make a sacrifice to aid in conjuring and commanding a creature
      called with planar ally, planar binding, or a similar spell. A sacrifice
      can be used in a variety of ways.</p><p><strong>Bargain</strong>: Making a
      sacrifice directly to the conjured being grants you a bonus on opposed
      Charisma checks made to compel the creature into service for the next
      hour.</p><p><strong>Enticement</strong>: Making a sacrifice the round
      before conjuring increases the DC of the Will save an outsider must
      attempt to resist being conjured.</p><p>Payment: Making a sacrifice
      directly to the conjured being allows you to pay for one service from the
      creature in commodities other than
      gold.</p><p><strong>Reinforcement</strong>: Making a sacrifice the round
      before creating a magic circle and preparing a summoning diagram amplifies
      the power of its warding magic, increasing the DC of Charisma checks the
      creature might attempt to escape. This lasts 1 day.</p><p>Multiple
      sacrifices can be made to affect a single conjuring, but the bonuses
      provided by this spell do not stack. Therefore, while you can make
      sacrifices to aid in conjuring and bargaining with a creature, you cannot
      make multiple sacrifices (even of varying types) to enhance the same
      effect for a particular conjuration. A sacrifice can consist of any kind
      of commodity the target creature favors, including living creatures,
      treasures, or more ephemeral offerings. While this spell is not
      fundamentally evil, good-aligned creatures are more selective in what
      offerings they accept, typically scoffing at blood sacrifices. Many
      sacrifices are fundamentally evil acts, such as murdering a pious innocent
      to conjure a fiend. Any creature might reject certain types of sacrifices,
      thus denying you the benefits of this spell, as the offering must appeal
      to the target—few outsiders would care for 2,000 gp worth of parchment,
      while 2,000 gp of diamonds would be widely coveted. The GM determines what
      sacrifices creatures find appealing.</p><p>The table below lists a number
      of likely offerings, along with the bonus such gifts provide and the
      offering’s equivalent value in gold pieces for the purposes of planar
      ally. Several of these sacrifices involve the loss of ability scores,
      levels, or lives, and some can cause changes in alignment. Any change
      wrought by such sacrifices (loss of ability score or level, or change in
      alignment) cannot be recovered, cured, or undone by any spell or effect
      short of miracle or wish. The same is true of creatures killed as a
      sacrifice; such creatures cannot be resurrected by any magic less powerful
      than these spells. Any object sacrificed with this spell is effectively
      destroyed or removed to an extraplanar holding of the conjured creature’s
      choice. The bonuses and values noted on the sacrifice effects table are
      guidelines for offerings; certain types of treasures or lives might prove
      especially valuable to specific creatures, with extraordinary sacrifices
      (such as a potent artifact or the life of a high-level paladin) garnering
      increased bonuses.</p><p>You cannot make greater sacrifices than those
      noted on the table to gain increased bonuses or gold values. For example,
      you could not gain 2 permanent negative levels to gain a +16 bonus, nor
      gain increased benefit from slaying 20 Hit Dice worth of creatures to pay
      for a 10-HD creature’s
      service.</p><p></p><table><thead><tr><th>Type</th><th>Sacrifice</th><th>Granted
      Bonus</th><th>GP
      Value</th></tr></thead><tbody><tr><td>Treasures</td><td>100 gp/HD of
      target</td><td>+1</td><td>Equal</td></tr><tr><td>Lives<sup>1</sup></td><td>One
      living creature with HD equal to target</td><td>+2</td><td>200
      gp/HD</td></tr><tr><td>Body/Mind<sup>2</sup></td><td>Reduction of ability
      scores by 1 reduced</td><td>+4</td><td>500
      gp/point</td></tr><tr><td>Morals<sup>2</sup></td><td>Alignment shifts one
      step toward target's</td><td>+6</td><td>1,000
      gp/step</td></tr><tr><td>Soul<sup>1</sup></td><td>One permanent negative
      level</td><td>+8</td><td>2,500
      gp</td></tr></tbody></table><p></p><p><sup>1 </sup>When used to sacrifice
      a life, body, mind, or soul other than the caster's own, this is an evil
      act.</p><p><sup>2</sup> A character can sacrifice only her own morals, and
      can do so only once per lifetime.</p>
  descriptors:
    value:
      - mindAffecting
  learnedAt:
    class:
      arcanist: 4
      cleric: 4
      oracle: 4
      sorcerer: 4
      warpriest: 4
      wizard: 4
  level: 4
  materials:
    value: see text
  school: enc
  sr: false
  subschool: charm
type: spell

